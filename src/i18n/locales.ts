import en from './translations/en';
import pl from './translations/pl';

export default {
	en,
	pl,
};
