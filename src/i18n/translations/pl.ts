export default {
	name: 'polski',
	authentication: {
		SignIn: 'Zaloguj się',
		SignUp: 'Zarejestruj się',
		CreateAccount: 'Utwórz konto',
		Login: 'Nazwa użytkownika lub email',
		ForgotPassword: 'Nie pamiętasz hasła?',
		AlreadyHaveAccount: 'Posiadasz już konto?',
		FirstName: 'Imię',
		LastName: 'Nazwisko',
		Email: 'E-mail',
		Username: 'Nazwa użytkownika',
		Password: 'Hasło',
		ConfirmPassword: 'Powtórz',
		RecoveryAccount: 'Przywracanie konta',
		RecoveryPasswordEmail:
			'Wyślimy na Twój adres email instrukcje przywracania konta',
		Confirm: 'Zatwierdź',
		GoBack: 'Powrót',
	},
};
