export default {
	name: 'English',
	authentication: {
		SignIn: 'Sign in',
		SignUp: 'Sign Up',
		CreateAccount: 'Create an account',
		Login: 'Username or email',
		ForgotPassword: "Don't remember a password?",
		AlreadyHaveAccount: 'Already have an account?',
		FirstName: 'First name',
		LastName: 'Last name',
		Email: 'E-mail',
		Username: 'Username',
		Password: 'Password',
		ConfirmPassword: 'Confirm',
		RecoveryAccount: 'Recovery your account',
		RecoveryPasswordEmail:
			'We will send the account recovery instructions to your email address',
		Confirm: 'Confirm',
		GoBack: 'Go back',
	},
};
