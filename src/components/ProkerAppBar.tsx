import { AppBar, Avatar, Box, Toolbar } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import { indigo } from '@mui/material/colors';
import React from 'react';

const ProkerAppBar = () => {
	return (
		<AppBar
			position="fixed"
			sx={{
				bgcolor: '#343434',
				zIndex: (theme) => theme.zIndex.drawer + 1,
			}}
		>
			<Toolbar>
				<RouterLink to={{ pathname: '/' }}>
					<img
						src="assets/proker-logo.svg"
						style={{ height: '36px' }}
					/>
				</RouterLink>
				<Box sx={{ flex: 1 }} />
				<Avatar sx={{ bgcolor: indigo[400] }}>MJ</Avatar>
			</Toolbar>
		</AppBar>
	);
};

export default ProkerAppBar;
