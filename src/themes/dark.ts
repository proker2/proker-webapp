import {createTheme} from '@mui/material';

export default createTheme({
	palette: {
		mode: 'dark',
		background: {
			default: '#2f2f2f',
			paper: '#1f1f1f',
		},
	},
});
