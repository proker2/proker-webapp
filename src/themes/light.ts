import {createTheme} from '@mui/material';

export default createTheme({
	palette: {
		mode: 'light',
		background: {
			default: '#eceff1',
			paper: '#fff',
		},
	},
});
