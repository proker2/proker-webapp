import React from 'react';
import { RouteObject } from 'react-router-dom';
import AuthenticationLayout from '../layouts/authentication/AuthenticationLayout';
import SignInPage from '../pages/authentication/SignInPage';
import SignUpPage from '../pages/authentication/SignUpPage';
import ResetPasswordPage from '../pages/authentication/ResetPasswordPage';
import MainLayout from '../layouts/main/MainLayout';
import ProjectPage from '../pages/project/ProjectPage';
import ProjectSettingsPage from '../pages/project/ProjectSettingsPage';
import DashboardPage from '../pages/dashboard/DashboardPage';
import ProjectsPage from '../pages/project/ProjectsPage';
import WorkspaceLayout from '../layouts/workspace/WorkspaceLayout';
import WorkspacePage from '../pages/workspace/WorkspacePage';

export default [
	{
		element: <MainLayout />,
		children: [
			{ path: '/', element: <DashboardPage /> },
			{ path: '/projects', element: <ProjectsPage /> },
			{ path: '/project', element: <ProjectPage /> },
			{ path: '/project/settings', element: <ProjectSettingsPage /> },
			{
				element: <WorkspaceLayout />,
				children: [{ path: '/workspace', element: <WorkspacePage /> }],
			},
		],
	},
	{
		element: <AuthenticationLayout />,
		children: [
			{ path: '/sign-in', element: <SignInPage /> },
			{ path: '/sign-up', element: <SignUpPage /> },
			{ path: '/reset-password', element: <ResetPasswordPage /> },
		],
	},
] as RouteObject[];
