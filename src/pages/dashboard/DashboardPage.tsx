import {
	Avatar,
	Box,
	Button,
	Card,
	CardActions,
	CardContent,
	Container,
	darken,
	Divider,
	Grid,
	IconButton,
	Stack,
	TextField,
	ToggleButton,
	ToggleButtonGroup,
	Typography,
	useTheme,
} from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import AppsIcon from '@mui/icons-material/Apps';
import ViewListIcon from '@mui/icons-material/ViewList';

const DashboardPage = () => {
	const theme = useTheme();
	return (
		<Box pt={3} mb={2}>
			<Container maxWidth="lg">
				<Grid container spacing={2}>
					<Grid item xs={12}>
						<Grid
							container
							justifyContent="space-between"
							alignItems="center"
							spacing={2}
						>
							<Grid item xs={12} sm="auto">
								<Typography variant="h5">
									Dzień dobry, Michał
								</Typography>
							</Grid>
							<Grid item xs={12} sm="auto">
								<Stack
									direction="row"
									alignItems="center"
									spacing={2}
									sx={{
										[theme.breakpoints.down('md')]: {
											justifyContent: 'space-between',
										},
									}}
								>
									<TextField size="small" label="Nazwa" />
									<ToggleButtonGroup
										value="grid"
										size="small"
									>
										<ToggleButton value="grid">
											<AppsIcon />
										</ToggleButton>
										<ToggleButton value="lists">
											<ViewListIcon />
										</ToggleButton>
									</ToggleButtonGroup>
									<Button
										variant="contained"
										disableElevation
									>
										Nowy obszar roboczy
									</Button>
								</Stack>
							</Grid>
						</Grid>
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<Card variant="outlined" sx={{ cursor: 'pointer' }}>
							<CardContent>
								<Stack
									direction="row"
									justifyContent="space-between"
									alignItems="center"
								>
									<Stack
										direction="row"
										alignItems="center"
										spacing={2}
									>
										<Avatar
											src="assets/deyanix.jpg"
											variant="rounded"
											sx={{
												backgroundColor: darken(
													theme.palette.background
														.paper,
													0.03
												),
												borderWidth: '1px',
												borderStyle: 'solid',
												borderColor: 'rgba(0,0,0,0.1)',
											}}
										/>
										<Typography variant="h6">
											deyanix
										</Typography>
									</Stack>
									<IconButton size="small">
										<MoreVertIcon />
									</IconButton>
								</Stack>
							</CardContent>
							<Divider />
							<CardActions
								sx={{
									backgroundColor: darken(
										theme.palette.background.paper,
										0.05
									),
									justifyContent: 'flex-end',
								}}
							>
								<Stack
									direction="row"
									alignItems="center"
									spacing={0.5}
								>
									<Typography variant="subtitle2">
										2
									</Typography>
									<Typography variant="caption">
										projekty
									</Typography>
								</Stack>
							</CardActions>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<Card variant="outlined" sx={{ cursor: 'pointer' }}>
							<CardContent>
								<Stack
									direction="row"
									justifyContent="space-between"
									alignItems="center"
								>
									<Stack
										direction="row"
										alignItems="center"
										spacing={2}
									>
										<Avatar
											src="assets/andret.jpg"
											variant="rounded"
											sx={{
												backgroundColor: darken(
													theme.palette.background
														.paper,
													0.03
												),
												borderWidth: '1px',
												borderStyle: 'solid',
												borderColor: 'rgba(0,0,0,0.1)',
											}}
										/>
										<Typography variant="h6">
											Andret
										</Typography>
									</Stack>
									<IconButton size="small">
										<MoreVertIcon />
									</IconButton>
								</Stack>
							</CardContent>
							<Divider />
							<CardActions
								sx={{
									backgroundColor: darken(
										theme.palette.background.paper,
										0.05
									),
									justifyContent: 'flex-end',
								}}
							>
								<Stack
									direction="row"
									alignItems="center"
									spacing={0.5}
								>
									<Typography variant="subtitle2">
										14
									</Typography>
									<Typography variant="caption">
										projektów
									</Typography>
								</Stack>
							</CardActions>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<Card variant="outlined" sx={{ cursor: 'pointer' }}>
							<CardContent>
								<Stack
									direction="row"
									justifyContent="space-between"
									alignItems="center"
								>
									<Stack
										direction="row"
										alignItems="center"
										spacing={2}
									>
										<Avatar
											src="assets/proker-icon-alt.svg"
											variant="rounded"
											sx={{
												backgroundColor: darken(
													theme.palette.background
														.paper,
													0.03
												),
												borderWidth: '1px',
												borderStyle: 'solid',
												borderColor: 'rgba(0,0,0,0.1)',
											}}
										/>
										<Typography variant="h6">
											Proker
										</Typography>
									</Stack>
									<IconButton size="small">
										<MoreVertIcon />
									</IconButton>
								</Stack>
							</CardContent>
							<Divider />
							<CardActions
								sx={{
									backgroundColor: darken(
										theme.palette.background.paper,
										0.05
									),
									justifyContent: 'flex-end',
								}}
							>
								<Stack
									direction="row"
									alignItems="center"
									spacing={0.5}
								>
									<Typography variant="subtitle2">
										7
									</Typography>
									<Typography variant="caption">
										projektów
									</Typography>
								</Stack>
							</CardActions>
						</Card>
					</Grid>
					<Grid item xs={12} sm={6} md={4}>
						<Card
							variant="outlined"
							sx={{ cursor: 'pointer' }}
							unselectable="on"
						>
							<CardContent>
								<Stack
									direction="row"
									justifyContent="space-between"
									alignItems="center"
								>
									<Stack
										direction="row"
										alignItems="center"
										spacing={2}
									>
										<Avatar
											variant="rounded"
											sx={{
												backgroundColor: '#336bb6',
												borderWidth: '1px',
												borderStyle: 'solid',
												borderColor: 'rgba(0,0,0,0.1)',
												color: 'white',
											}}
										>
											R
										</Avatar>
										<Typography variant="h6">
											Recadel
										</Typography>
									</Stack>
									<IconButton size="small">
										<MoreVertIcon />
									</IconButton>
								</Stack>
							</CardContent>
							<Divider />
							<CardActions
								sx={{
									backgroundColor: darken(
										theme.palette.background.paper,
										0.05
									),
									justifyContent: 'flex-end',
								}}
							>
								<Stack
									direction="row"
									alignItems="center"
									spacing={0.5}
								>
									<Typography variant="subtitle2">
										10
									</Typography>
									<Typography variant="caption">
										projektów
									</Typography>
								</Stack>
							</CardActions>
						</Card>
					</Grid>
				</Grid>
			</Container>
		</Box>
	);
};

export default DashboardPage;
