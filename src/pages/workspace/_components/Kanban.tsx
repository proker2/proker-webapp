import { useCallback, useRef, useState } from 'react';
import { Grid } from '@mui/material';
import KanbanContainer from './KanbanContainer';
import {
	arrayMove,
	horizontalListSortingStrategy,
	SortableContext,
	sortableKeyboardCoordinates,
} from '@dnd-kit/sortable';
import {
	closestCenter,
	CollisionDetection,
	DndContext,
	DragOverEvent,
	DragStartEvent,
	getFirstCollision,
	KeyboardSensor,
	MeasuringStrategy,
	PointerSensor,
	pointerWithin,
	rectIntersection,
	TouchSensor,
	useSensor,
	useSensors,
} from '@dnd-kit/core';
import { DragEndEvent, UniqueIdentifier } from '@dnd-kit/core/dist/types';
import { restrictToHorizontalAxis, restrictToWindowEdges } from '@dnd-kit/modifiers';
import _ from 'lodash';

export interface KanbanItemModel<E = any> {
	id: string;
	name: string;
}

export interface KanbanContainerModel<E = any> {
	id: string;
	name: string;
	items: KanbanItemModel[];
}

export interface KanbanProps {
	containers: KanbanContainerModel[];
}

export function Kanban(props: KanbanProps) {
	const [draggedId, setDraggedId] = useState<string | null>(null);
	const [items, setItems] = useState<KanbanContainerModel[]>(
		props.containers
	);
	const [clonedItems, setClonedItems] = useState<
		KanbanContainerModel[]
	>();
	const lastOverId = useRef<string>();

	const sensors = useSensors(
		useSensor(PointerSensor),
		useSensor(TouchSensor),
		useSensor(KeyboardSensor, {
			coordinateGetter: sortableKeyboardCoordinates,
		})
	);

	const collisionDetectionStrategy: CollisionDetection = useCallback(
		(args) => {
			if (draggedId && findContainerById(draggedId)) {
				return closestCenter({
					...args,
					droppableContainers: args.droppableContainers.filter(
						(container) => findContainerById(container.id)
					),
				});
			}

			const pointerIntersections = pointerWithin(args);
			const intersections =
				pointerIntersections.length > 0
					? pointerIntersections
					: rectIntersection(args);
			let overId = getFirstCollision(intersections, 'id');

			if (!_.isNil(overId)) {
				const container = findContainerById(overId);
				if (!_.isNil(container)) {
					const containerItems = container.items;

					// If a container is matched and it contains items (columns 'A', 'B', 'C')
					if (containerItems.length > 0) {
						// Return the closest droppable within that container
						overId = closestCenter({
							...args,
							droppableContainers:
								args.droppableContainers.filter(
									(container) =>
										container.id !== overId &&
										containerItems
											.map((item) => item.id)
											.includes(container.id)
								),
						})[0]?.id;
					}
				}
				lastOverId.current = overId;
				return [{ id: overId }];
			}

			// If no droppable is matched, return the last matcha
			return !_.isNil(lastOverId.current) ? [{ id: lastOverId.current }] : [];
		},
		[draggedId, items]
	);

	function findItemById(id: string): KanbanItemModel | undefined {
		return _.chain(items)
			.flatMap((container) => container.items)
			.find({ id })
			.value();
	}

	function findContainerById(id: string): KanbanContainerModel | undefined {
		return _.find(items, { id });
	}

	function findContainerByItem(id: string): KanbanContainerModel | undefined {
		return _.find(items, { items: [{ id }] });
	}

	function findContainer(id: string): KanbanContainerModel | undefined {
		return findContainerById(id) || findContainerByItem(id);
	}

	function handleDragStart(event: DragStartEvent) {
		setDraggedId(event.active.id);
		setClonedItems(_.cloneDeep(items));
	}

	function handleDragOver(event: DragOverEvent) {
		const { active, over } = event;
		if (!over || findContainerById(active.id)) {
			return;
		}

		const activeContainer = findContainer(active.id);
		const overContainer = findContainer(over.id);
		if (!overContainer || !activeContainer) {
			return;
		}

		if (activeContainer !== overContainer) {
			const activeItems = activeContainer.items;
			const overItems = overContainer.items;
			const activeIndex = _.findIndex(activeItems, { id: active.id });
			const overIndex = _.findIndex(overItems, { id: over.id });

			let newIndex: number;
			if (over.id in items) {
				newIndex = overItems.length + 1;
			} else {
				const isBelowOverItem =
					over &&
					active.rect.current.translated &&
					active.rect.current.translated.top >
						over.rect.top + over.rect.height;

				const modifier = isBelowOverItem ? 1 : 0;

				newIndex =
					overIndex >= 0
						? overIndex + modifier
						: overItems.length + 1;
			}

			activeContainer.items = activeItems.filter(
				(item) => item.id !== active.id
			);
			overContainer.items = [
				...overItems.slice(0, newIndex),
				activeItems[activeIndex],
				...overItems.slice(newIndex),
			];
		}
	}

	function handleDragEnd(event: DragEndEvent) {
		const activeId = event.active.id;
		const overId = event.over?.id;
		if (findContainerById(activeId) && overId) {
			setItems((items) => {
				const activeIndex = _.findIndex(
					items,
					(container) => container.id === activeId
				);
				const overIndex = _.findIndex(
					items,
					(container) => container.id === overId
				);
				return arrayMove(items, activeIndex, overIndex);
			});
		}

		const activeContainer = findContainer(activeId);
		if (!activeContainer || !overId) {
			setDraggedId(null);
			return;
		}

		const overContainer = findContainer(overId);
		if (overContainer) {
			const activeIndex = activeContainer.items.findIndex(
				(item) => item.id === activeId
			);
			const overIndex = overContainer.items.findIndex(
				(item) => item.id == overId
			);
			if (activeIndex !== overIndex) {
				overContainer.items = arrayMove(
					overContainer.items,
					activeIndex,
					overIndex
				);
			}
		}
		setDraggedId(null);
	}

	function handleDragCancel() {
		if (_.isArray(clonedItems)) {
			setItems(clonedItems);
			setClonedItems(undefined);
		}
		setDraggedId(null);
	}

	return (
		<DndContext
			measuring={{
				droppable: {
					strategy: MeasuringStrategy.Always,
				},
			}}
			sensors={sensors}
			modifiers={[restrictToWindowEdges]}
			collisionDetection={collisionDetectionStrategy}
			onDragStart={handleDragStart}
			onDragOver={handleDragOver}
			onDragEnd={handleDragEnd}
			onDragCancel={handleDragCancel}
		>
			<SortableContext
				items={items.map((item) => item.id)}
				strategy={horizontalListSortingStrategy}
			>
				<Grid container spacing={2}>
					{items.map((container) => (
						<Grid key={container.id} item xs={3}>
							<KanbanContainer container={container} />
						</Grid>
					))}
				</Grid>
			</SortableContext>
		</DndContext>
	);
}

export default Kanban;
