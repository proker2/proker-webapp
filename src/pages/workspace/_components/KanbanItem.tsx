import { Paper } from '@mui/material';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import { KanbanItemModel } from './Kanban';

export interface KanbanItemProps {
	item: KanbanItemModel;
}

export function KanbanItem(props: KanbanItemProps) {
	const { attributes, listeners, setNodeRef, transform, transition } =
		useSortable({ id: props.item.id });

	const style = {
		padding: 2,
		zIndex: 1300,
		cursor: 'move',
		transform: CSS.Transform.toString(
			transform && {
				x: transform.x,
				y: transform.y,
				scaleX: 1,
				scaleY: 1,
			}
		),
		transition,
	};

	return (
		<Paper
			ref={setNodeRef}
			variant="outlined"
			style={style}
			{...attributes}
			{...listeners}
		>
			{props.item.name}
		</Paper>
	);
}

export default KanbanItem;
