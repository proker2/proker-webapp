import {
	SortableContext,
	useSortable,
	verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { Box, Divider, Paper, Stack, Typography } from '@mui/material';
import KanbanItem from './KanbanItem';
import { CSS } from '@dnd-kit/utilities';
import { KanbanContainerModel } from './Kanban';

export interface KanbanContainerProps {
	container: KanbanContainerModel;
}

export function KanbanContainer(props: KanbanContainerProps) {
	const { attributes, listeners, setNodeRef, transform, transition } =
		useSortable({ id: props.container.id });

	const style = {
		cursor: 'move',
		transform: CSS.Transform.toString(
			transform && {
				x: transform.x,
				y: transform.y,
				scaleX: 1,
				scaleY: 1,
			}
		),
		transition,
	};

	return (
		<Paper
			ref={setNodeRef}
			variant="outlined"
			style={style}
			{...attributes}
			{...listeners}
		>
			<SortableContext
				id={props.container.id}
				items={props.container.items}
				strategy={verticalListSortingStrategy}
			>
				<Box bgcolor="#e0e0e0" paddingY={1}>
					<Typography variant="subtitle1" align="center">
						{props.container.name}
					</Typography>
				</Box>
				<Divider />
				<Stack spacing={2} margin={1}>
					{props.container.items.map((item) => (
						<KanbanItem key={item.id} item={item} />
					))}
				</Stack>
			</SortableContext>
		</Paper>
	);
}

export default KanbanContainer;
