import React, { useState } from 'react';
import Kanban from './_components/Kanban';

const WorkspacePage = () => {
	const [containers, setContainers] = useState([
		{
			id: 'A',
			name: 'Column 1',
			items: [
				{ id: 'A1', name: 'Item 1' },
				{ id: 'A2', name: 'Item 2' },
				{ id: 'A3', name: 'Item 3' },
			],
		},
		{
			id: 'B',
			name: 'Column 2',
			items: [
				{ id: 'B1', name: 'Item 4' },
				{ id: 'B2', name: 'Item 5' },
			],
		},
		{
			id: 'C',
			name: 'Column 3',
			items: [],
		},
	]);

	return <Kanban containers={containers} />;
};

export default WorkspacePage;
