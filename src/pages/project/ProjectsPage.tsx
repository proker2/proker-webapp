import React from 'react';
import {
	Avatar,
	Box,
	Divider,
	Grid,
	Input,
	InputAdornment,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Paper,
	Tab,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Tabs,
	TextField,
	Typography,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { Link as RouterLink } from 'react-router-dom';

function stringToColor(string: string) {
	let hash = 0;
	let i;

	for (i = 0; i < string.length; i += 1) {
		hash = string.charCodeAt(i) + ((hash << 5) - hash);
	}

	let color = '#';

	for (i = 0; i < 3; i += 1) {
		const value = (hash >> (i * 8)) & 0xff;
		color += `00${value.toString(16)}`.substr(-2);
	}

	return color;
}

const rows = ['Proker', 'Eductor', 'Logor', 'ManagerBook', 'MyFeasts'];

export default function ProjectsPage() {
	return (
		<>
			<Box sx={{ bgcolor: '#f6f8fa' }}>
				<Box sx={{ maxWidth: '1200px' }} mx="auto">
					<Box px={3} pt={2}>
						<Grid
							container
							justifyContent="space-between"
							alignItems="flex-end"
						>
							<Grid
								item
								xs={12}
								sm="auto"
								sx={{ order: { xs: 2, sm: 1 } }}
							>
								<Tabs variant="fullWidth" value={0}>
									<Tab label="Projekty" />
									<Tab label="Grupy" />
								</Tabs>
							</Grid>
							<Grid
								item
								xs={12}
								sm="auto"
								py={1}
								sx={{ order: { xs: 1, sm: 2 } }}
							>
								<TextField
									size="small"
									placeholder="Szukaj..."
									fullWidth
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<SearchIcon />
											</InputAdornment>
										),
									}}
								/>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</Box>
			<Box sx={{ maxWidth: '1200px' }} mx="auto">
				<Box px={3}>
					<List>
						{rows.map((row, index) => (
							<>
								<ListItem>
									<ListItemIcon>
										<Avatar
											component={RouterLink}
											to="/project"
											variant="rounded"
											sx={{
												bgcolor: stringToColor(row),
												textDecoration: 'none',
											}}
										>
											{row.charAt(0)}
										</Avatar>
									</ListItemIcon>
									<ListItemText>
										<Typography
											component={RouterLink}
											to="/project"
											color="inherit"
											sx={{
												textDecoration: 'none',
											}}
										>
											{row}
										</Typography>
									</ListItemText>
								</ListItem>
								{index !== rows.length - 1 && <Divider />}
							</>
						))}
					</List>
				</Box>
			</Box>
		</>
	);
}
