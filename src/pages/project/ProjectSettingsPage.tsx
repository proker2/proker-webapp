import {
	Avatar,
	Container,
	Grid,
	List,
	ListItem,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Paper,
	Stack,
	Typography,
} from '@mui/material';
import React from 'react';
import StarIcon from '@mui/icons-material/Star';

export default function ProjectSettingsPage() {
	return (
		<Container maxWidth="lg" sx={{ paddingTop: 2 }}>
			<Stack direction="row" alignItems="center" spacing={2}>
				<Avatar
					variant="rounded"
					sx={{
						bgcolor: '#16c8b8',
					}}
				>
					M
				</Avatar>
				<Typography variant="h5">ManagerBook</Typography>
			</Stack>
			<Grid container pt={2}>
				<Grid item xs={12} md={4}>
					<Paper>
						<List>
							<ListItemButton component="a" href="#simple-list">
								<ListItemIcon>
									<StarIcon />
								</ListItemIcon>
								<ListItemText primary="Spam" />
							</ListItemButton>
						</List>
					</Paper>
				</Grid>
				<Grid item xs={12} md={8}>
					bbb
				</Grid>
			</Grid>
		</Container>
	);
}
