import React from 'react';
import {
	Avatar,
	Box,
	Button,
	Chip,
	Container,
	Grid,
	Icon,
	IconButton,
	Link,
	Stack,
	Tab,
	Tabs,
	Typography,
} from '@mui/material';
import SettingsIcon from '@mui/icons-material/Settings';

export default function ProjectPage() {
	return (
		<>
			<Box sx={{ bgcolor: '#f6f8fa' }} pt={3} mb={2}>
				<Container maxWidth="lg">
					<Box pb={1}>
						<Grid container justifyContent="space-between">
							<Grid item>
								<Stack
									direction="row"
									spacing={2}
									alignItems="center"
								>
									<Avatar
										variant="rounded"
										sx={{
											width: 64,
											height: 64,
											fontSize: 32,
											bgcolor: '#16c8b8',
										}}
									>
										M
									</Avatar>
									<Stack justifyContent="space-between">
										<Typography variant="h5">
											ManagerBook
										</Typography>
										<Typography variant="subtitle1">
											Project ID: 1
										</Typography>
									</Stack>
								</Stack>
							</Grid>
							<Grid item>
								<IconButton>
									<SettingsIcon />
								</IconButton>
							</Grid>
						</Grid>
					</Box>
					<Box>
						<Tabs variant="standard" scrollButtons={true} value={0}>
							<Tab label="Informacje" />
							<Tab label="Autorzy" />
							<Tab label="Dystrybucja" />
						</Tabs>
					</Box>
				</Container>
			</Box>
			<Container maxWidth="lg">
				<Grid container spacing={2} pb={4}>
					<Grid item xs={12} md={8}>
						<Box p={2}>
							Aplikacja opiera się na bazie książek wraz z ich
							spisami treści i skorowidzami. Użytkownik wybiera
							interesujące go pozycje, a następnie może przeszukać
							je pod kątem szukanego przezeń hasła przedmiotowego.
							Pomaga to w znalezieniu istotnych informacji w
							przypadku, gdy użytkownik jest w posiadaniu kilku
							książek o podobnej tematyce i chciałby szybko
							uzyskać informacje na dany temat. Baza spisów i
							skorowidzów nie jest ogólnodostępna, wymaga
							zbudowania takiej bazy.
						</Box>
					</Grid>
					<Grid item xs={12} md={4}>
						<Stack spacing={2} px={2} pt={1}>
							<Stack spacing={1}>
								<Box>
									<Grid
										container
										spacing={1}
										justifyContent="space-between"
										alignItems="center"
									>
										<Grid item xs={12} md="auto">
											<Typography
												variant="body2"
												fontWeight="bold"
											>
												Data powstania
											</Typography>
										</Grid>
										<Grid item xs={12} md="auto">
											<Typography
												variant="body2"
												color="#666"
											>
												24 stycznia 2020
											</Typography>
										</Grid>
									</Grid>
								</Box>
								<Box>
									<Grid
										container
										spacing={1}
										justifyContent="space-between"
										alignItems="center"
									>
										<Grid item xs={12} md="auto">
											<Typography
												variant="body2"
												fontWeight="bold"
											>
												Przeznaczenie
											</Typography>
										</Grid>
										<Grid item xs={12} md="auto">
											<Typography
												variant="body2"
												color="#666"
											>
												do użytku własnego
											</Typography>
										</Grid>
									</Grid>
								</Box>
								<Box>
									<Grid
										container
										spacing={1}
										justifyContent="space-between"
										alignItems="center"
									>
										<Grid item xs={12} md="auto">
											<Typography
												variant="body2"
												fontWeight="bold"
											>
												Status
											</Typography>
										</Grid>
										<Grid item xs={12} md="auto">
											<Chip
												size="small"
												label="Wspierany"
												color="primary"
											/>
										</Grid>
									</Grid>
								</Box>
								<Box>
									<Grid
										container
										spacing={1}
										justifyContent="space-between"
										alignItems="center"
									>
										<Grid item xs={12} md="auto">
											<Typography
												variant="body2"
												fontWeight="bold"
											>
												Kod źródłowy
											</Typography>
										</Grid>
										<Grid item xs={12} md="auto">
											<Link
												href="#"
												variant="body2"
												underline="none"
											>
												<Stack
													direction="row"
													spacing={0.375}
													alignItems="center"
												>
													<div>gitlab.com</div>
													<Icon fontSize="inherit">
														launch
													</Icon>
												</Stack>
											</Link>
										</Grid>
									</Grid>
								</Box>
							</Stack>
							<Stack spacing={1}>
								<Typography variant="body1" fontWeight="bold">
									Autorzy
								</Typography>
								<Box>
									<Stack spacing={1.25}>
										<Box>
											<Grid
												container
												spacing={1}
												alignItems="center"
											>
												<Grid item>
													<Avatar src="assets/deyanix.jpg" />
												</Grid>
												<Grid item>deyanix</Grid>
											</Grid>
										</Box>
										<Box>
											<Grid
												container
												spacing={1}
												alignItems="center"
											>
												<Grid item>
													<Avatar src="assets/andret.jpg" />
												</Grid>
												<Grid item>Andret2344</Grid>
											</Grid>
										</Box>
									</Stack>
								</Box>
								<Button>2 więcej...</Button>
							</Stack>
							<Stack spacing={1}>
								<Typography variant="body1" fontWeight="bold">
									Technologie
								</Typography>
								<Box>
									<Grid container spacing={1}>
										<Grid item>
											<Chip size="small" label="Java" />
										</Grid>
										<Grid item>
											<Chip size="small" label="Spring" />
										</Grid>
										<Grid item>
											<Chip size="small" label="MySQL" />
										</Grid>
										<Grid item>
											<Chip size="small" label="React" />
										</Grid>
										<Grid item>
											<Chip
												size="small"
												label="MaterialUI"
											/>
										</Grid>
										<Grid item>
											<Chip size="small" label="SSR" />
										</Grid>
									</Grid>
								</Box>
							</Stack>
							<Stack spacing={1}>
								<Typography variant="body1" fontWeight="bold">
									Etykiety
								</Typography>
								<Box>
									<Grid container spacing={1}>
										<Grid item>
											<Chip
												size="small"
												label="library"
												sx={{ backgroundColor: '#fda' }}
											/>
										</Grid>
										<Grid item>
											<Chip
												size="small"
												label="plugin"
												sx={{
													backgroundColor: '#d9ffaa',
												}}
											/>
										</Grid>
									</Grid>
								</Box>
							</Stack>
							<Stack spacing={1}>
								<Typography variant="body1" fontWeight="bold">
									Platformy
								</Typography>
								<Box>
									<Grid container spacing={1}>
										<Grid item>
											<Chip size="small" label="iOS" />
										</Grid>
										<Grid item>
											<Chip
												size="small"
												label="Windows"
											/>
										</Grid>
										<Grid item>
											<Chip
												size="small"
												label="Android"
											/>
										</Grid>
										<Grid item>
											<Chip size="small" label="Linux" />
										</Grid>
									</Grid>
								</Box>
							</Stack>
						</Stack>
					</Grid>
				</Grid>
			</Container>
		</>
	);
}
