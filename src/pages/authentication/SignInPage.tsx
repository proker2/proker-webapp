import {
	Box,
	Button,
	Grid,
	Link,
	TextField,
	Typography,
} from '@mui/material';
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function SignInPage() {
	const { t } = useTranslation('authentication');

	return (
		<Box>
			<Grid container direction="column" spacing={3} p={4}>
				<Grid item>
					<Box sx={{ typography: 'body1' }}>
						<Typography variant="h5" align="center" pb={4}>
							{t('SignIn')}
						</Typography>
						<Grid container spacing={2}>
							<Grid xs={12} item>
								<TextField
									label={t('Login')}
									variant="outlined"
									fullWidth
								/>
							</Grid>
							<Grid xs={12} item>
								<TextField
									type="password"
									label={t('Password')}
									variant="outlined"
									fullWidth
								/>
								<Box mt={1}>
									<Link
										component={RouterLink}
										to="/reset-password"
										variant="caption"
										underline="none"
									>
										{t('ForgotPassword')}
									</Link>
								</Box>
							</Grid>
						</Grid>
					</Box>
				</Grid>
				<Grid item>
					<Grid
						container
						direction="row"
						justifyContent="space-between"
						alignItems="center"
						spacing={2}
					>
						<Grid
							item
							xs={12}
							sm="auto"
							order={{ xs: 2, sm: 1 }}
							sx={{ marginLeft: { sm: '-8px' } }}
						>
							<Button
								component={RouterLink}
								to="/sign-up"
								variant="text"
								color="primary"
								fullWidth
							>
								{t('CreateAccount')}
							</Button>
						</Grid>
						<Grid item xs={12} sm="auto" order={{ xs: 1, sm: 2 }}>
							<Button
								variant="contained"
								color="primary"
								disableElevation
								fullWidth
							>
								{t('SignIn')}
							</Button>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</Box>
	);
}
