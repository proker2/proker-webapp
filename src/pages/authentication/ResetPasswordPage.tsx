import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import React from 'react';
import { useTranslation } from 'react-i18next';

export default function ResetPasswordPage() {
	const { t } = useTranslation('authentication');

	return (
		<Box>
			<Grid container direction="column" spacing={3} p={4}>
				<Grid item>
					<Box sx={{ typography: 'body1' }}>
						<Typography variant="h5" align="center" pb={1}>
							{t('RecoveryAccount')}
						</Typography>
						<Typography variant="body1" color="grey.600" pb={3}>
							{t('RecoveryPasswordEmail')}
						</Typography>
						<Grid container spacing={2}>
							<Grid xs={12} item>
								<TextField
									label={t('Email')}
									variant="outlined"
									fullWidth
								/>
							</Grid>
						</Grid>
					</Box>
				</Grid>
				<Grid item>
					<Grid
						container
						direction="row"
						justifyContent="space-between"
						alignItems="center"
						spacing={2}
					>
						<Grid
							item
							xs={12}
							sm="auto"
							order={{ xs: 2, sm: 1 }}
							sx={{ marginLeft: { sm: '-8px' } }}
						>
							<Button
								component={RouterLink}
								to="/sign-up"
								variant="text"
								color="primary"
								fullWidth
							>
								{t('GoBack')}
							</Button>
						</Grid>
						<Grid item xs={12} sm="auto" order={{ xs: 1, sm: 2 }}>
							<Button
								variant="contained"
								color="primary"
								disableElevation
								fullWidth
							>
								{t('Confirm')}
							</Button>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</Box>
	);
}
