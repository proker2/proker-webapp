import {
	Box,
	Button,
	Grid,
	Link,
	TextField,
	Typography,
} from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link as RouterLink } from 'react-router-dom';

export default function SignUpPage() {
	const { t } = useTranslation('authentication');

	return (
		<Box>
			<Grid container direction="column" spacing={3} p={4}>
				<Grid item>
					<Box sx={{ typography: 'body1' }}>
						<Typography variant="h5" align="center" pb={4}>
							{t('SignUp')}
						</Typography>
						<Grid container spacing={2}>
							<Grid xs={12} sm={6} item>
								<TextField
									label={t('FirstName')}
									variant="outlined"
									fullWidth
									size="small"
								/>
							</Grid>
							<Grid xs={12} sm={6} item>
								<TextField
									label={t('LastName')}
									variant="outlined"
									fullWidth
									size="small"
								/>
							</Grid>
							<Grid xs={12} item>
								<TextField
									label={t('Username')}
									variant="outlined"
									fullWidth
									size="small"
								/>
							</Grid>
							<Grid xs={12} item>
								<TextField
									label={t('Email')}
									variant="outlined"
									fullWidth
									size="small"
								/>
							</Grid>
							<Grid xs={12} sm={6} item>
								<TextField
									type="password"
									label={t('Password')}
									variant="outlined"
									fullWidth
									size="small"
								/>
							</Grid>
							<Grid xs={12} sm={6} item>
								<TextField
									type="password"
									label={t('ConfirmPassword')}
									variant="outlined"
									fullWidth
									size="small"
								/>
							</Grid>
						</Grid>
					</Box>
				</Grid>
				<Grid item>
					<Grid
						container
						direction="row"
						justifyContent="space-between"
						alignItems="center"
						spacing={2}
					>
						<Grid item xs={12} sm="auto" order={{ xs: 2, sm: 1 }}>
							<Link
								component={RouterLink}
								to="/sign-in"
								variant="caption"
								underline="none"
							>
								{t('AlreadyHaveAccount')}
							</Link>
						</Grid>
						<Grid item xs={12} sm="auto" order={{ xs: 1, sm: 2 }}>
							<Button
								variant="contained"
								color="primary"
								disableElevation
								fullWidth
							>
								{t('SignUp')}
							</Button>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</Box>
	);
}
