import React from 'react';
import {
	Box,
	Grid,
	IconButton,
	MenuItem,
	PaletteMode,
	Paper,
	Select,
	ThemeProvider,
} from '@mui/material';
import { Outlet } from 'react-router-dom';
import { DarkMode, LightMode } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import lightTheme from '../../themes/light';
import darkTheme from '../../themes/dark';
import AuthenticationSelect from './components/AuthenticationSelect';

export default function AuthenticationLayout(
	props: React.PropsWithChildren<{}>
) {
	const { i18n } = useTranslation();
	const [mode, setMode] = React.useState<PaletteMode>(() =>
		window.matchMedia &&
		window.matchMedia('(prefers-color-scheme: dark)').matches
			? 'dark'
			: 'light'
	);
	const [language, setLanguage] = React.useState<string>(
		() =>
			i18n.language.match(/(\w{2})(-\w{2})?/)?.[1] ||
			String(i18n.options.fallbackLng)
	);

	const theme = React.useMemo(
		() => (mode === 'light' ? lightTheme : darkTheme),
		[mode]
	);

	function toggleColorMode() {
		setMode(mode === 'light' ? 'dark' : 'light');
	}

	function handleChangeLanguage(event: SelectChangeEvent) {
		const value: string = event.target.value;
		if (value) {
			i18n.changeLanguage(value).then(() => setLanguage(value));
		}
	}

	return (
		<ThemeProvider theme={theme}>
			<Box
				sx={{
					minHeight: '100vh',
					bgcolor: 'background.default',
				}}
			>
				<Box sx={{ maxWidth: 440 }} mx="auto" p={3}>
					<Box mx="auto" sx={{ maxWidth: 200 }} py={4}>
						<img
							src={
								theme.palette.mode === 'dark'
									? 'assets/proker-logo.svg'
									: 'assets/proker-logo-alt.svg'
							}
							style={{ width: '100%' }}
						/>
					</Box>
					<Paper variant="outlined">
						<Outlet />
					</Paper>
					<Grid
						container
						alignItems="center"
						justifyContent="space-between"
						mt={2}
					>
						<Select
							variant="standard"
							value={language}
							input={<AuthenticationSelect />}
							onChange={handleChangeLanguage}
						>
							<MenuItem value="en">English</MenuItem>
							<MenuItem value="pl">Polish</MenuItem>
						</Select>
						<IconButton onClick={toggleColorMode}>
							{theme.palette.mode === 'light' ? (
								<DarkMode />
							) : (
								<LightMode />
							)}
						</IconButton>
					</Grid>
				</Box>
			</Box>
		</ThemeProvider>
	);
}
