import {InputBase, styled} from '@mui/material';

export default styled(InputBase)`
	margin-left: -10px;

	.MuiSelect-select {
		padding-left: 10px;
		padding-right: 10px;
	}

	&.MuiInput-root:before {
		content: none;
	}
`;
