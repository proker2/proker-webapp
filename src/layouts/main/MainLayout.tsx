import { Box, ThemeProvider, Toolbar } from '@mui/material';
import React from 'react';
import { Outlet } from 'react-router-dom';
import lightTheme from '../../themes/light';
import ProkerAppBar from '../../components/ProkerAppBar';

export default function MainLayout() {
	return (
		<ThemeProvider theme={lightTheme}>
			<ProkerAppBar />
			<Box
				sx={{
					minHeight: '100vh',
					bgcolor: 'background.default',
				}}
			>
				<Box>
					<Toolbar />
					<Outlet />
				</Box>
			</Box>
		</ThemeProvider>
	);
}
