import React from 'react';
import {
	Avatar,
	Box,
	CSSObject,
	Divider,
	Drawer,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Stack,
	styled,
	Theme,
	Toolbar,
	Typography,
	useTheme,
} from '@mui/material';
import { Outlet } from 'react-router-dom';
import InboxIcon from '@mui/icons-material/Inbox';
import MailIcon from '@mui/icons-material/Mail';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';

const drawerWidth = 240;

const openedMixin = (theme: Theme): CSSObject => ({
	width: drawerWidth,
	transition: theme.transitions.create('width', {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.enteringScreen,
	}),
	overflowX: 'hidden',
});

const closedMixin = (theme: Theme): CSSObject => ({
	transition: theme.transitions.create('width', {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	overflowX: 'hidden',
	width: `calc(${theme.spacing(7)} + 1px)`,
});

const MinimizableDrawer = styled(Drawer, {
	shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
	width: drawerWidth,
	flexShrink: 0,
	whiteSpace: 'nowrap',
	boxSizing: 'border-box',
	...(open && {
		...openedMixin(theme),
		'& .MuiDrawer-paper': openedMixin(theme),
	}),
	...(!open && {
		...closedMixin(theme),
		'& .MuiDrawer-paper': closedMixin(theme),
	}),
	'& .MuiListItemIcon-root': {
		minWidth: '48px',
	},
}));

const WorkspaceLayout = () => {
	const theme = useTheme();
	const [open, setOpen] = React.useState(true);

	return (
		<Box sx={{ display: 'flex' }}>
			<MinimizableDrawer
				open={open}
				variant="permanent"
				// sx={{
				// 	width: 200,
				// 	flexShrink: 0,
				// 	[`& .MuiDrawer-paper`]: {
				// 		width: 200,
				// 		boxSizing: 'border-box',
				// 	},
				// }}
			>
				<Toolbar />
				<Stack
					direction="column"
					justifyContent="space-between"
					sx={{ height: '100%' }}
				>
					<Box>
						<Box px={1} py={2}>
							<Stack
								direction="row"
								alignItems="center"
								spacing={2}
							>
								<Avatar
									src="assets/proker-icon-alt.svg"
									variant="rounded"
									sx={{
										width: '40px',
										height: '40px',
										borderWidth: '1px',
										borderStyle: 'solid',
										borderColor: 'rgba(0,0,0,0.1)',
									}}
								/>
								<Typography variant="h6">Proker</Typography>
							</Stack>
						</Box>
						<Divider />
						<List>
							{['Inbox', 'Starred', 'Send email', 'Drafts'].map(
								(text, index) => (
									<ListItem button key={text}>
										<ListItemIcon>
											{index % 2 === 0 ? (
												<InboxIcon />
											) : (
												<MailIcon />
											)}
										</ListItemIcon>
										<ListItemText primary={text} />
									</ListItem>
								)
							)}
							<Divider />
							<ListItem button onClick={() => setOpen(!open)}>
								<ListItemIcon>
									{open ? (
										<ChevronLeftIcon />
									) : (
										<ChevronRightIcon />
									)}
								</ListItemIcon>
								<ListItemText primary="Ukryj" />
							</ListItem>
						</List>
					</Box>
				</Stack>
			</MinimizableDrawer>
			<Box component="main" sx={{ flexGrow: 1, p: 3 }}>
				<Outlet />
			</Box>
		</Box>
	);
};

export default WorkspaceLayout;
